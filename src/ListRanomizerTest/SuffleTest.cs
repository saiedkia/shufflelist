using ListRandomizer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace ListRanomizerTest
{
    [TestClass]
    public class ShuffleTest

    {
        [TestMethod]
        public void SyncTest()
        {
            List<string> items = new List<string>();
            for(int i = 0; i < 500; i++)
                items.Add(Faker.Name.First());


            List<string> result = items.ToList();
            result.Shuffle();


            Assert.AreNotEqual(items, result);
        }

        [TestMethod]
        public async void ASyncTest()
        {
            List<string> items = new List<string>();
            for (int i = 0; i < 500; i++)
                items.Add(Faker.Name.First());


            List<string> result = items.ToList();
            await result.ShuffleAsync();


            Assert.AreNotEqual(items, result);
        }
    }
}
