﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ListRandomizer
{
    public static class ListExtension
    {
        public static void Shuffle<T>(this List<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static Task<List<T>> ShuffleAsync<T>(this List<T> list)
        {
            return Task.Run(() =>
            {
                Thread thread = new Thread(new ParameterizedThreadStart(ShuffleData));
                thread.Start();
                thread.Join();

                return list;
            });
        }

        private static void ShuffleData(object list)
        {
            List<object> _list = (List<object>)list;
            Random rng = new Random();
            int n = _list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                object value = _list[k];
                _list[k] = _list[n];
                _list[n] = value;
            }
        }
    }
}
